package data_structures.test;

import junit.framework.*;
import model.data_structures.*;
import model.vo.*;

public class StackTest<T extends Comparable <T>> extends TestCase {

	private Stack<Taxi> stack;
	private Stack<Taxi> stack2;
	
	@Before
	private void setupEscenario1(){
		
		stack = new Stack<Taxi>();
		
		Taxi taxi1 = new Taxi("A123","A");
		Taxi taxi2 = new Taxi("C345","D");
		Taxi taxi3 = new Taxi("R567","F");
		Taxi taxi4 = new Taxi("T678","J");
		Taxi taxi5 = new Taxi("Y901","P");
		
		stack.push(taxi1);
		stack.push(taxi2);
		stack.push(taxi3);
		stack.push(taxi4);
		stack.push(taxi5);
	}
	
	private void setupEscenario2(){
		stack2 = new Stack<Taxi>();
	}
	
	@test
	public void testPush(T elemento) {
		setupEscenario1();
		Taxi taxiC = new Taxi("ZZ32","K");
		stack.push(taxiC);
		assertEquals("el numero de taxis es incorrecto",6,stack.getLongitud());
		assertEquals(stack.darPrimero(), taxiC);

		
		}
	}

	public void testPop() {
		setupEscenario1();
		Taxi taxiC = new Taxi("A123","A");
		stack.pop();
		assertEquals("el numero de taxis es incorrecto",4,stack.getLongitud());
		assertTrue("el primer taxi no es el correcto", !stack.darPrimero().equals(taxiC));
		
		setupEscenario2();
		stack2.pop();
		assertTrue("No deber�a retornar algo porque la pila est� vac�a.", !stack.darPrimero().equals(null));
		}
	}

	public void testIsEmpty() {
		setupEscenario1();

		assertTrue("La pila no est� vac�a.",stack.isEmpty()==true);

		}
		
		setupEscenario2();
		if(stack2.isEmpty()==false){
			fail("La pila est� vac�a.");
		}
	}

	public void testGetLongitud(){
		setupEscenario1();
		if(stack.getLongitud()!=5){
			fail("El tama�o de la pila no es el correcto");
		}
	}
}