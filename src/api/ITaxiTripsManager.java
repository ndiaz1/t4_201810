package api;

import model.vo.RangoFechaHora;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Method to load the services of a specific taxi 
	 * The services are loaded in both a Stack and a Queue 
	 * @param servicesFile - path to the JSON file with taxi services 
	 */
	public void loadServices(String serviceFile, RangoFechaHora pRangoFechaHora);
}
