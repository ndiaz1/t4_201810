package model.logic;


import api.ITaxiTripsManager;
import model.data_structures.*;
import model.vo.*;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	private Service[] servicios;
	private Taxi[] taxis;
	private Compania[] companias;

	public void loadServices(String direccionJson, RangoFechaHora pRangoFechaHora) {
		List<Service> lista = new List<Service>();
		List<Taxi> listaTaxis = new List<Taxi>();
		List<Compania> listaCompanias = new List<Compania>();

		System.out.println("Cargando datos de  " + direccionJson);
		System.out.println("\n-------------------------------------------------------");
		JsonParser parser = new JsonParser();
		try{
			JsonArray arr = (JsonArray) parser.parse(new FileReader(direccionJson));
			for (int i = 0; arr != null && i < arr.size(); i++){
				JsonObject obj= (JsonObject) arr.get(i);

				String id = "Unknown";
				if ( obj.get("taxi_id") != null ){
					id = obj.get("taxi_id").getAsString();}

				String comp = "Independent Owner";
				if ( obj.get("company") != null ){ 
					comp = obj.get("company").getAsString();}

				String dca = "Unknown";
				if ( obj.get("dropoff_community_area") != null ){
					dca = obj.get("dropoff_community_area").getAsString(); }

				String pca = "Unknown";
				if ( obj.get("pickup_community_area") != null ){
					pca = obj.get("pickup_community_area").getAsString(); }

				String tripid = "Unknown";
				if ( obj.get("trip_id") != null ){
					tripid = obj.get("trip_id").getAsString(); }

				String tripst = "Unknown";
				if ( obj.get("trip_start_timestamp") != null ){
					tripst = obj.get("trip_start_timestamp").getAsString(); }

				String tripet = "Unknown";
				if ( obj.get("trip_end_timestamp") != null ){
					tripet = obj.get("trip_end_timestamp").getAsString(); }

				int tripseconds = -1;
				if ( obj.get("trip_seconds") != null ){
					tripseconds = obj.get("trip_seconds").getAsInt(); }

				double tripmiles = -1;
				if ( obj.get("trip_miles") != null ){
					tripmiles = obj.get("trip_miles").getAsDouble(); }

				double triptotal = -1;
				if ( obj.get("trip_total") != null ){ 
					triptotal = obj.get("trip_total").getAsDouble(); }

				//Carga de servicios
				String[] tiempoInicio = tripst.split("T");
				String[] tiempoFinal = tripet.split("T");

				if(pRangoFechaHora.getFechaInicial().compareTo(tiempoInicio[0])<=0&&pRangoFechaHora.getHoraInicio().compareTo(tiempoInicio[1])<=0&&pRangoFechaHora.getFechaFinal().compareTo(tiempoFinal[0])>=0&&pRangoFechaHora.getHoraFinal().compareTo(tiempoFinal[1])>=0)
				{
					Service nuevo = new Service(id, comp, dca, pca, tripid, tripst, tripet, tripseconds, tripmiles, triptotal);
					lista.agregar(nuevo);
				}
			}

			//Carga de taxis de acuerdo a servicios cargados
			Nodo<Service> actual = lista.darPrimero();
			while(actual!=null){
				if(!actual.darElemento().getTaxiId().equals("Unknown"))
				{
					Taxi temp = new Taxi(actual.darElemento().getTaxiId(),actual.darElemento().getCompany());
					Taxi existente = listaTaxis.elementoExistente(temp);
					if(existente == null){
						listaTaxis.agregar(temp);
						listaTaxis.darUltimo().darElemento().agregarServicio(actual.darElemento());
					}
					else{
						existente.agregarServicio(actual.darElemento());
					}
				}
				actual = actual.darSiguiente();
			}
			//Carga de companias de acuerdo a taxis cargados.
			Nodo<Taxi> actualC = listaTaxis.darPrimero();
			while(actualC!=null){
				Compania temp = new Compania(actualC.darElemento().getCompany());
				Compania existente = listaCompanias.compExistente(temp);
				if(existente == null){
					temp.agregarTaxi(actualC.darElemento());
					listaCompanias.agregar(temp);
				}
				else{
					existente.agregarTaxi(actualC.darElemento());
				}
				actualC = actualC.darSiguiente();
			}


			//Carga de datos en arreglos
			int longitudServicios = lista.darLongitud();
			int longitudTaxis = listaTaxis.darLongitud();
			int longitudCompanias = listaCompanias.darLongitud();
			servicios = new Service[longitudServicios];
			taxis = new Taxi[longitudTaxis];
			companias = new Compania[longitudCompanias];

			Nodo<Service> actualA = lista.darPrimero();
			int indiceService = 0;
			while(actualA!=null){
				Service sActual= actualA.darElemento();
				servicios[indiceService]=sActual;
				actualA = actualA.darSiguiente();
				indiceService++;
			}

			Nodo<Taxi> actualB = listaTaxis.darPrimero();
			int indiceTaxi=0;
			while(actualB!=null){
				Taxi sActualB= actualB.darElemento();
				taxis[indiceTaxi]=sActualB;
				actualB = actualB.darSiguiente();
				indiceTaxi++;
			}

			Nodo<Compania> actualComp = listaCompanias.darPrimero();
			int indiceCompania=0;
			while(actualComp!=null){
				Compania sActualComp= actualComp.darElemento();
				companias[indiceCompania]=sActualComp;
				actualComp = actualComp.darSiguiente();
				indiceCompania++;
			}
			System.out.println("\nSe cargaron "+servicios.length+" servicios.");
			System.out.println("Se cargaron "+taxis.length+" taxis.");
			System.out.println("Se cargaron "+companias.length+" companias.");
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();}
	}
	//Punto 1
	public Taxi[] taxisPorId(){
		Heap<Taxi> heap = new Heap<Taxi>();
		Taxi[] t = taxis;
		heap.sort(t);
		return t;
	}

	//Punto 2
	public Compania[] companiasPorNumeroServicios(){
		Heap<Compania> heap = new Heap<Compania>();
		Compania[] c = companias;
		heap.sort(c);
		return c;
	}


}