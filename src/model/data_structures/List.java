package model.data_structures;

import model.vo.Compania;

public class List<T extends Comparable<T>> implements LinkedList<T> {

	/**
	 * Primer nodo de la lista 
	 */
	private Nodo<T> primero;

	/**
	 * Ultimo nodo de la lista.
	 */
	private Nodo<T> ultimo;

	/**
	 * Longitud de la lista.
	 */
	private int longitud;

	/**
	 * Crea una nueva lista sin elementos.
	 */
	public List(){
		primero = null;
		ultimo = null;
		longitud = 0;
	}

	/**
	 * Retorna el primer nodo de la lista.
	 * @return primer nodo de la lista.
	 */
	public Nodo<T> darPrimero(){
		return primero;
	}

	/**
	 * Retorna el �ltimo nodo de la lista
	 * @return �ltimo nodo de la lista.
	 */
	public Nodo<T> darUltimo(){
		return ultimo;
	}

	/**
	 * Retorna la longitud de la lista.
	 * @return Longitud de la lista.
	 */
	public int darLongitud()
	{
		return longitud;
	}

	/**
	 * Agrega un nuevo nodo a la lista con el elemento dado por par�metro.
	 */
	public boolean agregar(T n)
	{
		boolean agrego = false;
		Nodo<T> nuevo = new Nodo<T>(n);
		if(primero == null)
		{
			primero = nuevo;
			ultimo = nuevo;
			longitud++;
			agrego = true;
		}
		else
		{
			ultimo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimo);
			ultimo = nuevo;
			longitud ++;
			agrego = true;
		}
		return agrego;
	}
	public void cambiarPrimero(Nodo<T> p) {
		primero=p;
	}

	public boolean agregarEnOrden(T n){
		boolean agrego = false;
		Nodo<T> nuevo = new Nodo<T>(n);
		if(primero == null)
		{
			primero = nuevo;
			ultimo = nuevo;
			longitud++;
			agrego = true;
		}
		else if(primero.darElemento().compareTo(n)>0){
			nuevo.cambiarSiguiente(primero);
			primero.cambiarAnterior(nuevo);
			primero = nuevo;
			longitud++;
			agrego = true;
		}
		else{
			Nodo<T> actual = this.darPrimero();
			while(actual.darSiguiente()!=null&&!agrego){
				if(actual.darSiguiente().darElemento().compareTo(n)>0){
					nuevo.cambiarSiguiente(actual.darSiguiente());
					nuevo.cambiarAnterior(actual);
					actual.cambiarSiguiente(nuevo);
					longitud++;
					agrego = true;
				}
				actual = actual.darSiguiente();
			}
			if(!agrego){
				ultimo.cambiarSiguiente(nuevo);
				nuevo.cambiarAnterior(ultimo);
				ultimo = nuevo;
				longitud ++;
				agrego = true;
			}
		}
		return agrego;
	}

	/**
	 * Elimina el nodo con el elemento dado por par�metro.
	 */
	public boolean eliminar(T e)
	{
		boolean elim = false;
		if(e.compareTo(e)==0 && !elim)
		{
			primero = primero.darSiguiente();
			elim = true;
			longitud --;
		}
		else
		{
			Nodo<T> actual = primero.darSiguiente();
			while(actual.darSiguiente() != null && !elim)
			{
				if(actual.darSiguiente().darElemento().compareTo(e)==0)
				{
					actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
					elim = true;
					longitud--;
				}
				else
				{
					actual = actual.darSiguiente();
				}
			}
		}
		return elim;
	}

	public boolean isEmpty(){
		return longitud==0;
	}

	public T elementoExistente(T elem){
		Nodo<T> actual = this.darPrimero();
		while(actual!=null){
			if(actual.darElemento().compareTo(elem)==0){
				return actual.darElemento();
			}
			actual = actual.darSiguiente();
		}
		return null;
	}

	public Compania compExistente(Compania elem){
		Nodo<Compania> actual = (Nodo<Compania>) this.darPrimero();
		while(actual!=null){
			if(actual.darElemento().compareByName(elem)==0){
				return actual.darElemento();
			}
			actual = actual.darSiguiente();
		}
		return null;
	}
}