//Adaptado de http://www.code2learn.com/2011/09/heapsort-array-based-implementation-in.html
package model.data_structures;

public class Heap <T extends Comparable<T>> {
	private T[] a;
	private int n;
	private int izq;
	private int der;
	private int may;

	public void crearPQ(T[] a){
		n= a.length-1;
		for(int i= n/2 ;i>=0 ;i--){
			mayor(a,i);
		}
	}
	public void mayor(T[] a, int k){ 
		izq=2*k;
		der=2*k+1;
		if(izq <= n && a[izq].compareTo(a[k])>0){
			may=izq;
		}
		else{
			may=k;
		}

		if(der <= n && a[der].compareTo(a[may])>0){
			may=der;
		}
		if(may!=k){
			exchange(k,may);
			mayor(a, may);
		}
	}

	public void exchange(int i, int j){
		T t = a[i];
		a[i]=a[j];
		a[j]=t; 
	}

	public void sort(T []a0){
		a=a0;
		crearPQ(a);

		for(int i=n;i>0;i--){
			exchange(0, i);
			n=n-1;
			mayor(a, 0);
		}
	}
}