package model.vo;

import model.data_structures.*;

public class Compania implements Comparable<Compania> {

	private String nombre;

	private List<Taxi> taxisInscritos;	

	public Compania (String pNombre){
		nombre = pNombre;
		taxisInscritos = new List<Taxi>();
	}

	public String getNombre() {
		return nombre;
	}

	public List<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void agregarTaxi(Taxi nuevo) {
		this.taxisInscritos.agregar(nuevo);
	}
	public int getNumeroServicios(){
		int numSer = 0;
		Nodo<Taxi> actual = this.getTaxisInscritos().darPrimero();
		while(actual != null){
			Taxi sActual = actual.darElemento();
			numSer=numSer+sActual.darNumeroServicios();
			actual = actual.darSiguiente();
		}
		return numSer;
	}
	public int compareByName(Compania o) {
		//Compara las compa�ias por nombre
		int res = 0;
		if(this.nombre.compareTo(o.getNombre())>0){
			res = 1;
		}
		else if(this.nombre.compareTo(o.getNombre())<0){
			res = -1;
		}
		return res;
	}

	public int compareTo(Compania o) {
		//Compara las compa�ias por numero de servicios 
		int res = 0;
		if(this.getNumeroServicios()>o.getNumeroServicios()){
			res = -1;
		}
		else if(this.getNumeroServicios()<o.getNumeroServicios()){
			res = 1;
		}
		return res;
	}
}