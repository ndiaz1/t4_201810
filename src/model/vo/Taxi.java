package model.vo;
import model.data_structures.*;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	private String taxi_id;
	private String company;
	private List<Service> servicios;

	public Taxi(String id, String comp){
		taxi_id=id;
		company=comp;
		servicios=new List<Service>();
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany()
	{
		return company;
	}

	public List<Service> getServicios(){
		return servicios;
	}

	public void agregarServicio(Service pServicio){
		servicios.agregar(pServicio);
	}

	public int darNumeroServicios(){
		return servicios.darLongitud();
	}

	public double darDistanciaTotal(){
		double distancia = 0;
		Nodo<Service> actual = servicios.darPrimero();
		while(actual!=null){
			Service sActual = actual.darElemento();
			if(sActual.getTripMiles()!=-1){
				distancia= distancia + sActual.getTripMiles();
			}
			actual = actual.darSiguiente();
		}
		return distancia;
	}

	public double darGananciaTotal(){
		double ganancia = 0;
		Nodo<Service> actual = servicios.darPrimero();
		while(actual!=null){
			Service sActual = actual.darElemento();
			if(sActual.getTripTotal()!=-1){
				ganancia= ganancia + sActual.getTripMiles();
			}
			actual = actual.darSiguiente();
		}
		return ganancia;
	}

	public double darRentabilidad(){
		double ganancia = this.darGananciaTotal();
		double distancia = this.darDistanciaTotal();
		if(distancia !=0){
			return ganancia/distancia;
		}
		else{
			return ganancia;
		}
	}
	public List<Service> serviciosPorRTiempoTaxi(RangoFechaHora rango){
		List<Service> Cservicios= null;
		Nodo<Service> actual= servicios.darPrimero();
		String rangoI=rango.getFechaInicial()+"T"+rango.getHoraInicio();
		String rangoF=rango.getFechaFinal()+"T"+rango.getHoraFinal();
		while(!actual.darSiguiente().equals(null)) {
			int compare= actual.darElemento().getStartTimeC().compareTo(rangoI);
			if(compare>0) {
				return Cservicios;
			}				
			if(actual.darElemento().getStartTime().equals(rangoI)){
				Cservicios.agregar(actual.darElemento());			
			}				
			if(actual.darElemento().getStartTimeC().compareTo(rangoI)<=0 && actual.darElemento().getEndTimeC().compareTo(rangoF)>=0) {
				Cservicios.agregar(actual.darElemento());
			}
			actual=actual.darSiguiente();
		}
		return Cservicios;
	}

	public double darPalataRango(RangoFechaHora rango) {
		List<Service> x=serviciosPorRTiempoTaxi( rango);
		double total=0;
		Nodo<Service> j= x.darPrimero();
		while(j.darSiguiente()!=null) {
			total+=j.darElemento().getTripTotal();
			j=j.darSiguiente();
		}
		return total;
	}

	public double darDistanciaRango(RangoFechaHora rango) {
		List<Service> x=serviciosPorRTiempoTaxi( rango);
		double total=0;
		Nodo<Service> j= x.darPrimero();
		while(j.darSiguiente()!=null) {
			total+=j.darElemento().getTripMiles();
			j=j.darSiguiente();
		}
		return total;
	}
	public double darDuracionRango(RangoFechaHora rango) {
		List<Service> x=serviciosPorRTiempoTaxi( rango);
		double total=0;
		Nodo<Service> j= x.darPrimero();
		while(j.darSiguiente()!=null) {
			total+=j.darElemento().getTripSeconds();
			j=j.darSiguiente();
		}
		return total;
	}

	public double darFacturacion(RangoFechaHora rango){
		double facturacion = 0 ;
		Nodo<Service> actual = servicios.darPrimero();
		while(actual!=null){
			Service sActual = actual.darElemento();
			if(sActual.getStartTime()[0].compareTo(rango.getFechaInicial())>=0 && sActual.getStartTime()[1].compareTo(rango.getHoraInicio())>=0 && sActual.getEndTime()[0].compareTo(rango.getFechaFinal())<=0 && sActual.getEndTime()[1].compareTo(rango.getHoraFinal())<=0)
			{
				facturacion+= actual.darElemento().getTripTotal();	
			}
			actual = actual.darSiguiente();
		}
		return facturacion;
	}
	public int numServicios(RangoFechaHora rango) {
		int i=0;
		Nodo<Service>h=servicios.darPrimero();
		String rangoI=rango.getFechaInicial()+"T"+rango.getHoraInicio();
		String rangoF=rango.getFechaFinal()+"T"+rango.getHoraFinal();
		while(h.darSiguiente()!=null) {
			if(h.darElemento().getStartTimeC().compareTo(rangoI)<=0 && h.darElemento().getEndTimeC().compareTo(rangoF)>=0) {
				i++;
			}
			h=h.darSiguiente();
		}
		return i;
	}
	/**
	 * Compara por taxi_id
	 */
	public int compareTo(Taxi o) 
	{
		int res = 0;
		if(this.getTaxiId().compareTo(o.getTaxiId())>0){
			res = 1;
		}
		else if(this.getTaxiId().compareTo(o.getTaxiId())<0){
			res = -1;
		}
		return res;
	}	
}