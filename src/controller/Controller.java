package controller;

import model.logic.TaxiTripsManager;
import model.vo.Taxi;
import model.vo.RangoFechaHora;
import model.vo.Service;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static TaxiTripsManager  manager = new TaxiTripsManager();

	/** To load the services of the taxi with taxiId */
	public static void loadServices( String serviceFile, RangoFechaHora pRangoFechaHora ) {
		manager.loadServices( serviceFile, pRangoFechaHora);
	}
	public static Comparable[] taxisPorId(){
		return manager.taxisPorId();
	}
	public static Comparable[] companiasPorNumeroServicios(){
		return manager.companiasPorNumeroServicios();
	}
}
