package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.*;
import model.logic.TaxiTripsManager;
import model.vo.*;

/**
 * view del programa
 */
public class TaxiTripsManagerView {

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;

					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}
				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicial = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicial = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinal = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinal = sc.next();

				RangoFechaHora rfh = new RangoFechaHora(fechaInicial, fechaFinal, horaInicial, horaFinal);

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.loadServices(linkJson, rfh);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB\n");

				break;
			case 2:
				//Punto 1
				Taxi[] taxiP1 = (Taxi[]) Controller.taxisPorId();
				for(int r = 0; r<taxiP1.length;r++){
					Taxi actual = taxiP1[r];
					System.out.println(actual.getTaxiId()+" - Servicios: "+actual.getServicios().darLongitud());
				}
				break;
			case 3:
				//Punto 2
				Compania[] compP2 =  (Compania[]) Controller.companiasPorNumeroServicios();
				for(int r = 0; r<compP2.length;r++){
					Compania actual = compP2[r];
					System.out.println("Servicios: "+actual.getNumeroServicios()+ " - "+actual.getNombre()+" - Taxis: "+actual.getTaxisInscritos().darLongitud());
				}
				break;
			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("\n---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------\n");
		System.out.println("1. Cargar datos.");
		System.out.println("2. Punto 1: Retorne e imprima los taxis resultado del ordenamiento mostrando su identificador y su numero de servicios asociados.");
		System.out.println("3. Punto 2: Retorne e imprima la informacion de las empresas en el orden de prioridad dado por su total de servicios en la consulta inicial.");
		System.out.println("\nType the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- �Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
